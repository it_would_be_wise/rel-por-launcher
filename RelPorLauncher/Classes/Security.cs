﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Security.Cryptography.Xml;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Xml;
using System.Xml.Linq;

namespace RelPorLauncher.Classes
{
    public class Security
    {
        public static RSACryptoServiceProvider CryptoSP = new RSACryptoServiceProvider();

        public static void Initialize()
        {
            CryptoSP.FromXmlString(Constants.PublicKey);
        }

        public static XmlDocument ToXmlDocument(XDocument xDocument)
        {
            var xmlDocument = new XmlDocument();
            using (var xmlReader = xDocument.CreateReader())
            {
                xmlDocument.PreserveWhitespace = false; // this could be problematic
                xmlDocument.Load(xmlReader);
            }
            return xmlDocument;
        }

        // from How to: Verify the Digital Signatures of XML Documents
        // https://msdn.microsoft.com/en-us/library/ms229950(v=vs.110).aspx
        // Verify the signature of an XML file against an asymmetric 
        // algorithm and return the result.
        public static bool VerifyXml(string url, XDocument XDoc)
        {
            try
            {
                XmlDocument Doc = ToXmlDocument(XDoc);

                // Check arguments.
                if (Doc == null)
                    throw new ArgumentException("Doc");

                // Create a new SignedXml object and pass it
                // the XML document class.
                SignedXml signedXml = new SignedXml(Doc);

                // Find the "Signature" node and create a new
                // XmlNodeList object.
                XmlNodeList nodeList = Doc.GetElementsByTagName("Signature");

                // Throw an exception if no signature was found.
                if (nodeList.Count <= 0)
                {
                    throw new CryptographicException("Verification failed: No Signature was found in the document.");
                }

                // This example only supports one signature for
                // the entire XML document.  Throw an exception 
                // if more than one signature was found.
                if (nodeList.Count >= 2)
                {
                    throw new CryptographicException("Verification failed: More that one signature was found for the document.");
                }

                // Load the first <signature> node.  
                signedXml.LoadXml((XmlElement)nodeList[0]);

                // Check the signature and return the result.
                if (!signedXml.CheckSignature(CryptoSP))
                {
                    throw new Exception("Signature did not match!");
                }

                return true;
            }
            catch (Exception e)
            {
                MessageBox.Show("Error encounterd while validating XML file at " + url + " ( please report to " + Constants.ReportErrorsToAddress + " ): \n" + e.Message);
            }
            return false;
        }
    }
}
