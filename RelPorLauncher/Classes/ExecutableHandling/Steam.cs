﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Windows;
using RelPorLauncher.Models;

namespace RelPorLauncher.Classes
{
    public class Steam
    {
        private static readonly List<string> KnownExeStrings = new List<string>
        {
            @"UOS.exe",
            @"Steam.exe",
            @"UOSteam.exe"
        };

        public static bool CheckInstalled()
        {
            return KnownExeStrings.Any(exestring => File.Exists(Path.Combine(Config.Instance.SteamPath, exestring)));
        }

        public static string GetExeString()
        {
            return
                KnownExeStrings.FirstOrDefault(
                    exestring => File.Exists(Path.Combine(Config.Instance.SteamPath, exestring)));
        }

        public static void RunSteam()
        {
            try
            {

                /*
                // paths enterred like this
                [HKEY_CURRENT_USER\Software\AssistUO]

[HKEY_CURRENT_USER\Software\AssistUO\127.0.0.1]

[HKEY_CURRENT_USER\Software\AssistUO\142.54.187.98]

[HKEY_CURRENT_USER\Software\AssistUO\192.99.36.190]

[HKEY_CURRENT_USER\Software\AssistUO\45.58.39.252]

[HKEY_CURRENT_USER\Software\AssistUO\goobergoober]

[HKEY_CURRENT_USER\Software\AssistUO\login.uoforever.com]
    */

                var process = new Process
                {
                    StartInfo = { FileName = Path.Combine(Config.Instance.SteamPath, GetExeString()), WorkingDirectory = Config.Instance.SteamPath }
                };

                process.Start();
            }
            catch (Exception)
            {
                MessageBox.Show(
                    "A valid installation of Steam could not be found. Please set the path to your Steam folder through the settings menu.");
            }
        }
    }
}