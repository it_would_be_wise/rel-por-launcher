﻿using System.Windows.Controls;

namespace RelPorLauncher.Views.SpecificSettingsViews
{
    /// <summary>
    ///     Interaction logic for GeneralSettings.xaml
    /// </summary>
    public partial class GeneralSettings : UserControl
    {
        public GeneralSettings()
        {
            InitializeComponent();
        }
    }
}